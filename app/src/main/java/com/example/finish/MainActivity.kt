package com.example.finish

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

class MainActivity : AppCompatActivity()
{

    private val titleList= mutableListOf<String>()
    private val descList= mutableListOf<String>()
    private val imageList= mutableListOf<Int>()

    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        postToList()
        val recyclerViewNew: RecyclerView = findViewById<RecyclerView>(R.id. recyclerView)
        recyclerViewNew.layoutManager = LinearLayoutManager(this)
        recyclerViewNew.adapter = RecyclerAdapter(titleList, descList, imageList)
    }

    private fun addToList(title: String, descriptor: String, image: Int)
    {
        titleList.add(title)
        descList.add(descriptor)
        imageList.add(image)
    }

    private fun postToList()
    {
        for (i in 1..1000 )
        {
            addToList("Title $i", "Description $i", R.drawable.baseline_unpublished_black_36)
        }
    }
}